package com.kw.database;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.kw.events.CityEvent;

public class DatabaseHelper extends SQLiteOpenHelper
{

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "eventManager";
	private static final String TABLE_EVENT = "event";
	private static final String KEY_EVENT_LOCATION = "eventLocation";
	//	private static final String KEY_EVENT_TIME = "eventTime";
	//	private static final String KEY_EVENT_DATE = "eventDate";
	private static final String KEY_EVENT_COST = "evevntCost";
	private static final String KEY_EVENT_DETAIL = "evevntDetail";
	private static final String KEY_EVENT_NAME = "eventName";
	//	private static final String KEY_EVENT_IMAGE = "eventImage";
	private static final String KEY_EVENT_ID = "eventId";
	private static final String KEY_EVENT_TRACK = "eventTrack";
	private static final String URL_STRING = "http://beta.json-generator.com/api/json/get/VJmn4kHex";

	EventCallback callback;

	public DatabaseHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		callback = (EventCallback) context;

		// TODO Auto-generated constructor stub
	}

	public void executeParser()
	{
		new EventParser().execute(URL_STRING);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_EVENT + "("
				+ KEY_EVENT_ID + " INTEGER PRIMARY KEY," + KEY_EVENT_NAME + " TEXT,"
				+ KEY_EVENT_DETAIL + " TEXT," + KEY_EVENT_COST + " TEXT, " + KEY_EVENT_LOCATION + " TEXT, " + 
				KEY_EVENT_TRACK + " INTEGER"+ ")";
		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub

	}

	public CityEvent getEventbyId(int id) 
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_EVENT, new String[] {KEY_EVENT_ID, KEY_EVENT_NAME,
				KEY_EVENT_DETAIL, KEY_EVENT_COST, KEY_EVENT_LOCATION, KEY_EVENT_TRACK }, 
				KEY_EVENT_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();
		CityEvent cityEvent = new CityEvent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4),cursor.getInt(5));
		Log.d("KeepWork", "Event Id: "+cursor.getInt(5));
		return cityEvent;
	}

	public List<CityEvent> getEventbyTrack(int id) 
	{
		List<CityEvent> cityEventList = new ArrayList<CityEvent>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_EVENT, new String[] {KEY_EVENT_ID, KEY_EVENT_NAME,
				KEY_EVENT_DETAIL, KEY_EVENT_COST, KEY_EVENT_LOCATION, KEY_EVENT_TRACK },
				KEY_EVENT_TRACK + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
		if (cursor != null  && cursor.moveToFirst())
			do
			{
				CityEvent cityEvent = new CityEvent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
						cursor.getString(4),cursor.getInt(5));
				cityEventList.add(cityEvent);
			} while(cursor.moveToNext());
		// return contact
		return cityEventList;
	}

	public List<CityEvent> getAllEvents()
	{
		List<CityEvent> cityEventList = new ArrayList<CityEvent>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_EVENT;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) 
		{
			do 
			{
				CityEvent cityEvent = new CityEvent();
				cityEvent.setEventId(Integer.parseInt(cursor.getString(0)));
				cityEvent.setEventName(cursor.getString(1));
				cityEvent.setEventDetail(cursor.getString(2));
				cityEvent.setEventCost(cursor.getString(3));
				cityEvent.setEventLocation(cursor.getString(4));
				// Adding contact to list
				cityEventList.add(cityEvent);
			} while (cursor.moveToNext());
		}

		//		callback.getEventDetailsFromDB(cityEventList);

		// return contact list
		return cityEventList;
	}

	public void addEvent(CityEvent event) 
	{
		Log.d("KeepWork", "Data: addEvent");
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_EVENT_NAME, event.getEventName()); 
		values.put(KEY_EVENT_LOCATION, event.getEventLocation()); 
		values.put(KEY_EVENT_COST, event.getEventCost());
		values.put(KEY_EVENT_DETAIL, event.getEventDetail());
		values.put(KEY_EVENT_TRACK, event.getEventTrack());
		Log.d("KeepWork", "Data: addEvent2");

		db.insert(TABLE_EVENT, null, values);
		db.close(); 
	}

	public void updateTrack(CityEvent event, int status)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_EVENT_NAME, event.getEventName()); 
		values.put(KEY_EVENT_LOCATION, event.getEventLocation()); 
		values.put(KEY_EVENT_COST, event.getEventCost());
		values.put(KEY_EVENT_DETAIL, event.getEventDetail());
		values.put(KEY_EVENT_TRACK, status);
		// updating row
		db.update(TABLE_EVENT, values, KEY_EVENT_ID + " = ?",
				new String[] { String.valueOf(event.getEventId()) });
		db.close();
	}


	class EventParser extends AsyncTask<String, Void, JSONArray>
	{

		HttpURLConnection connection = null;
		@Override
		protected JSONArray doInBackground(String... params) 
		{
			// TODO Auto-generated method stub
			try
			{
				URL url = new URL(params[0]);
				connection = (HttpURLConnection) url.openConnection();
				StringBuffer buffer = new StringBuffer(1024);
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String temp ="";
				while ((temp = reader.readLine())!=null) 
				{
					buffer.append(temp).append('\n');
				}
				reader.close();
				JSONArray array = new JSONArray(buffer.toString());
				Log.d("KeepWork", "Data: "+array.length());

				return array;
			}
			catch (Exception e) 
			{
				// TODO: handle exception
				Log.d("KeepWork", "Data: "+e.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(JSONArray result)
		{
			// TODO Auto-generated method stub
			if(result!=null)
			{
				for(int i = 0; i < result.length(); i++)
				{
					CityEvent event = new CityEvent();
					try 
					{
						event.setEventCost(result.getJSONObject(i).getString("cost"));
						event.setEventDetail(result.getJSONObject(i).getString("detail"));
						event.setEventLocation(result.getJSONObject(i).getString("location"));
						event.setEventName(result.getJSONObject(i).getString("name"));
						event.setEventTrack(0);
						//						event.setImage((result.getJSONObject(i).getString("name").replaceAll("[\\s&]", ""))+".png");


						/*						Log.d("Json Keep", "Image "+event.getImage());
						Log.d("Json Keep", "location "+event.getEventLocation());
						Log.d("Json Keep", "detail "+event.getEventDetail());
						Log.d("Json Keep", "cost "+event.getEventCost());*/
						addEvent(event);

					} catch (JSONException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			callback.eventComplete();
		}

	}

	public interface EventCallback
	{
		public void eventComplete();
	}

}
