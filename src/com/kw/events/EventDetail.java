package com.kw.events;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kw.database.DatabaseHelper;
import com.kw.database.DatabaseHelper.EventCallback;
import com.kw.keepwork.R;

public class EventDetail extends Activity implements EventCallback
{
	DatabaseHelper databaseHelper;
	TextView detailView;
	ToggleButton toggleButton;
	int position;
	CityEvent cityEvent;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_detail);
		databaseHelper = new DatabaseHelper(this);
		detailView = (TextView) findViewById(R.id.detail);
		toggleButton = (ToggleButton) findViewById(R.id.toggleButton1);



		Intent intent = this.getIntent();
		position = intent.getIntExtra("position", 0);

		cityEvent = databaseHelper.getEventbyId(position+1);

		if(cityEvent.getEventTrack() == 1 )
			toggleButton.setChecked(true);
		else
			toggleButton.setChecked(false);

		detailView.setText(cityEvent.getEventDetail());
	}

	public void OnToggle(View view)
	{
		boolean track = ((ToggleButton) view).isChecked();

		if(!track)
			databaseHelper.updateTrack(cityEvent, 0);
		else
			databaseHelper.updateTrack(cityEvent, 1);

	}

	@Override
	public void eventComplete() {
		// TODO Auto-generated method stub

	}

}
