package com.kw.events;

public class CityEvent 
{
	String eventName;
	String eventType;
	String eventTime;
	String eventCost;
	String eventLocation;
	String eventDetail;
	int eventTrack;
	String image;
	int eventId;


	public CityEvent()
	{

	}

	public CityEvent(String name, String detail, String cost, String location, int track)
	{
		eventCost = cost;
		eventDetail = detail;
		eventLocation = location;
		eventName = name;
		eventTrack = track;
	}

	public CityEvent(int id, String name, String detail, String cost, String location, int track)
	{
		eventId = id;
		eventCost = cost;
		eventDetail = detail;
		eventLocation = location;
		eventName = name;
		eventTrack = track;
	}
	public void setEventCost(String eventCost) {
		this.eventCost = eventCost;
	}

	public String getEventCost() {
		return eventCost;
	}

	public void setEventDetail(String eventDetail) {
		this.eventDetail = eventDetail;
	}

	public String getEventDetail() {
		return eventDetail;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public String getEventLocation() {
		return eventLocation;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventTrack(int eventTrack) {
		this.eventTrack = eventTrack;
	}

	public int getEventTrack()
	{
		return eventTrack;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getEventId() {
		return eventId;
	}
	
	/*public void setImage(String image) {
		this.image = image;
	}
	public String getImage() {
		return image;
	}*/

}
