package com.kw.events;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.kw.keepwork.R;

public class UserLogin extends Activity implements OnClickListener
{

	EditText editText;
	Button submit;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_login);
		editText = (EditText) findViewById(R.id.usrnam);
		submit = (Button) findViewById(R.id.submit);
		submit.setOnClickListener(this);

//		DatabaseHelper helper = new DatabaseHelper(this);
	}


	@Override
	public void onClick(View arg0) 
	{
		int id = arg0.getId();
		switch (id) {
		case R.id.submit:
		{
			SharedPreferences preferences = this.getSharedPreferences("Event", MODE_PRIVATE);
			String name = preferences.getString("name", "-1");
			if(!name.equalsIgnoreCase(editText.getText().toString()))
			{
				Editor  editor = preferences.edit();
				editor.putString("name", editText.getText().toString());
				editor.commit();
				Intent event = new Intent(UserLogin.this, EventList.class);
				startActivity(event);
				this.finish();
			}
			else
			{
				Intent event = new Intent(UserLogin.this, EventList.class);
				startActivity(event);
				this.finish();
			}
		}

		break;

		default:
			break;
		}

	}
}
