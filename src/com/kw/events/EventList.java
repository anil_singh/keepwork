package com.kw.events;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.kw.adapter.EventAdapter;
import com.kw.database.DatabaseHelper;
import com.kw.database.DatabaseHelper.EventCallback;
import com.kw.keepwork.R;

public class EventList extends ActionBarActivity implements EventCallback
{
	ListView eventListView;
	DatabaseHelper databaseHelper;
	List<CityEvent> event_List;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_list);
		eventListView = (ListView) findViewById(R.id.eventList);
		//		event_List = new ArrayList<CityEvent>();
		databaseHelper = new DatabaseHelper(EventList.this);


		getEventDetailsFromDB();
		eventListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) 
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent(EventList.this, EventDetail.class);
				intent.putExtra("position", arg2);
				startActivity(intent);
			}
		});

	}

	public void getEventDetailsFromDB()
	{
		event_List = databaseHelper.getEventbyTrack(1);;
		if(event_List.size() > 0)
		{
//			Log.d("KeepWork","Track list"+databaseHelper.getEventbyTrack(1).size());
			eventListView.setAdapter(new EventAdapter(EventList.this, event_List));
		}
		else 
		{
			event_List.clear();
			event_List = databaseHelper.getAllEvents();
			if(event_List.size() > 0)
				eventListView.setAdapter(new EventAdapter(EventList.this, event_List));
			else
				databaseHelper.executeParser();
		}
		Log.d("KeepWork", "Event List: "+event_List.size());
	}
	

	@Override
	public void eventComplete()
	{
		// TODO Auto-generated method stub
		getEventDetailsFromDB();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		
		getMenuInflater().inflate(R.menu.user_login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_all)
		{
			event_List.clear();
			event_List = databaseHelper.getAllEvents();
			eventListView.setAdapter(new EventAdapter(EventList.this, event_List));
		}
		else if(id == R.id.action_track)
		{
			event_List.clear();
			event_List = databaseHelper.getEventbyTrack(1);
			if(event_List.size() > 0)
			eventListView.setAdapter(new EventAdapter(EventList.this, event_List));
			else
			{
				Toast.makeText(this, "No Event Follow by you", Toast.LENGTH_LONG).show();
				event_List.clear();
				event_List = databaseHelper.getAllEvents();
				eventListView.setAdapter(new EventAdapter(EventList.this, event_List));
			}
		}
		return super.onOptionsItemSelected(item);
	}
	

}
