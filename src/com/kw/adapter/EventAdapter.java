package com.kw.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kw.events.CityEvent;
import com.kw.keepwork.R;

public class EventAdapter extends BaseAdapter
{

	Activity context;
	List<CityEvent> eventList;
//	CityEvent cityEvent;
//	EventAdapterListener eventListener;
	
	public EventAdapter(Activity context, List<CityEvent> eventList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
//		eventListener = (EventAdapterListener) context;
		this.eventList = eventList;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return eventList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView event_name, event_cost, event_loc;
		ImageView event_img;

		ViewHolder(View view)
		{
			event_name = (TextView) view.findViewById(R.id.eventnam);
			event_cost = (TextView) view.findViewById(R.id.cost);
			event_loc = (TextView) view.findViewById(R.id.loc);
			event_img = (ImageView) view.findViewById(R.id.eventImg);
		}

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.single_event_row, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}
		CityEvent cityEvent = eventList.get(position);
		holder.event_name.setText(cityEvent.getEventName());
		holder.event_loc.setText(cityEvent.getEventLocation());
		holder.event_cost.setText(cityEvent.getEventCost());
		holder.event_img.setImageResource(R.drawable.ic_launcher);
		
		return row;
	}
	
	/*public interface EventAdapterListener
	{
		public void eventDetail(int position);
	}*/

}
